package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.ExploredSpot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.MineMap;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.MineMap.MapCopyException;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.Spot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers.onepriority.CoordinatesForSpot;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.solvers.FinalAreaFlagger;

public class TestForRepeatedFlags {

	@Test
	public MineMap testFlagCompleteList() throws MapCopyException {
		MineMap map = new MineMap(5, 5, 5, 0);
		return map;
	}
	
	@Test
	public  void searchForOnes() throws MapCopyException {
		ArrayList<CoordinatesForSpot> fullListOfOnes = new ArrayList<CoordinatesForSpot>();
		MineMap map = testFlagCompleteList();
		for (int cc = 0; cc < map.cols; cc++) {
			for (int rc = 0; rc < map.rows; rc++) {
				ExploredSpot aSpot = map.getPos(rc, cc);
				if (Spot.SAFE.equals(aSpot.type)) {
					if (aSpot.nearMineCount == 1) {
						fullListOfOnes.add(new CoordinatesForSpot(rc, cc));
					}
				}
			}
		}
		FinalAreaFlagger.flagSpots(fullListOfOnes, map); 
		
	}

	// FinalAreaFlagger.flagSpots();
	@Test
	public void  flaggedCorrectly() throws MapCopyException {
		ArrayList<CoordinatesForSpot> flaggedCorrectly = new ArrayList<CoordinatesForSpot>();
		MineMap map = testFlagCompleteList();
		for (int cc = 0; cc < map.cols; cc++) {
			for (int rc = 0; rc < map.rows; rc++) {
				ExploredSpot a = map.getPos(rc, cc);
				if (Spot.FLAG.equals(a.type)) {
					flaggedCorrectly.add(new CoordinatesForSpot(rc, cc));
					assertEquals("Spot should be flagged.",Spot.FLAG, map.getPos(rc, cc)); 
				}
				
			}

		}
		
	}
}
