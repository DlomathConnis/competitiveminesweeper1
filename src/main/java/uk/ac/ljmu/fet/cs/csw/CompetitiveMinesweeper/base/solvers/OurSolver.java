package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers;

import java.util.Collections;

public class OurSolver extends AbstractSolver {
	/**
	 * Chooses a spot from the list at random then picks it on the map that it recieves.
	 * 
	 * 
	 * @param theMapToSolve		The Map which is used when picking a spot.	
	 * @param areasToPickFrom	The List of areas where the method can pick from.
	 * 
	 */
	
	
	public static void pickASpotFromList(MineMap theMapToSolve,
			List<coordinates> areasToPickFrom) {
		if (areasToPickFrom.size() == 0)
			return;
		Collections.shuffle(areasToPickFrom);
		coordinates toPick = areasToPickFrom.get(0);
		theMapToSolve.pickSpot(toPick.rowCoord, toPick.colCoord);
	}
}
