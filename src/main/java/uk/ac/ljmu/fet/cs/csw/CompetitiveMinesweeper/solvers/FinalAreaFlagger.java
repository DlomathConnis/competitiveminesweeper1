package uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.solvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import CompetitiveMinesweeper.base.solvers.AbstractSolver;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.MineMap;
import uk.ac.ljmu.fet.cs.csw.CompetitiveMinesweeper.base.solvers.onepriority.CoordinatesForSpot;

public class FinalAreaFlagger extends AbstractSolver {
	public void run() {
		// two boilerplate lines that is a strong requirement for almost all solvers
		super.run();
		MineMap theMapToSolve = getMyMap();

		// The actual algorithm starts here
		mainloop: do {
			List<CoordinatesForSpot> allOnes = searchForOnes(theMapToSolve);
			if (allOnes.isEmpty()) {
				// We have to go for a random choice, nothing is explored so far
				doFullAreaRandomPick(theMapToSolve);
			} else {
				// Randomise the spots with ones so we don't explore the map in a predefined
				// order
				Collections.shuffle(allOnes);
				List<List<CoordinatesForSpot>> unexploredLocationsAroundOnes = searchUnexploredAround(theMapToSolve,
						allOnes);
				int biggestSize = -1;
				List<CoordinatesForSpot> neighbourhoodToPickFrom = null;
				for (int i = 0; i < unexploredLocationsAroundOnes.size(); i++) {
					List<CoordinatesForSpot> aNeighbourhood = unexploredLocationsAroundOnes.get(i);
					CoordinatesForSpot neighbourhoodCentre = allOnes.get(i);
					if (areThereAnyFlagsAround(theMapToSolve, neighbourhoodCentre)) {
						// We can pick all the spots in the neighbourhood of a 1 if we already know
						// where is its mine (i.e., we have a flag on it)
						pickAllOnList(theMapToSolve, aNeighbourhood);
					} else {
						// We are not so lucky, we have no flags around, we have to look deeper
						if (aNeighbourhood.size() == 1) {
							// There is just one unexplored neighbouring spot, that must be a mine then,
							// let's flag it
							CoordinatesForSpot theMine = aNeighbourhood.get(0);
							theMapToSolve.flagASpot(theMine.rowCoord, theMine.colCoord);
							// As we now have an extra flag on the map, we are better of restarting the
							// loop.
							continue mainloop;
						} else {
							// We have multiple unexplored neighbours, we need to look for the biggest
							// unexplored area so we can pick a bit safer from them.
							if (biggestSize < aNeighbourhood.size()) {
								biggestSize = aNeighbourhood.size();
								neighbourhoodToPickFrom = aNeighbourhood;
							}
						}
					}
				}
				if (biggestSize > 1) {
					// We had found a spot with one on it where there are multiple unexplored
					// neighbours, we need to pick one of them.
					pickARandomSpotFromList(theMapToSolve, neighbourhoodToPickFrom);
				} else {
					// There are no areas to explore around the ones, we have to guess the next spot
					doFullAreaRandomPick(theMapToSolve);
				}
			}
		} while (!theMapToSolve.isEnded());
	}

	public static void flagSpots(ArrayList<CoordinatesForSpot> fullListOfOnes, MineMap map) {
		// TODO Auto-generated method stub
		
	}

}
